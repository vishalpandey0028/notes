
# Into to Linux

- Shell Types
  - Bourne Shell
  - C shell
  - Z shell
  - Bourne again Shell

commands
    echo (printing any string on cli)
    ls (listing all file and directory in current directory)
    cd (change directory/folder)
    pwd (present working directory/folder)
    mkdir (creating new directory/folder)
      - mkdir -p /tmp/aisa (-p option to create new directories recursively in one commend)

    rm (remove directory/ file)
        rm -r (for directory)
    cp  (copy file or directoy)
    touch (create new file)
    cat 
        cat > new_file (add content to file)
        cat ne_file (view content of file)
    mv (move / rename directory)
    whoami (current username)
    id (used id)
    su (switch user)
    curl url -O(to download)
    wget url -O save filename

Vi editor
    vi file_name
    esc key to switch to command mode
        arrow key to navigate
        i (insert mode)
        x (deletes word)
        :w (save file)
        :q(quit)
        :wq(save and quit)
        u(undo )
        dd(deletes entire line)
        yy(copy entire line)
        p(paste line)
        ctrl+u(scroll up) d(down)
        /of(finds all of) nkey (to navigate through searched values)

package manager
    for centos rpm
    rpm -i (install)
    rpm -e (remove)
    rpm -q (search installed packages)

    yum high level package manager
    yum install
    yum remove
    (/etc/yum.repos.d)
    yum repolist (show all repo list)
    yum list(installed packages list)
        --showduplicates

services

    service http start
    systemctl start httpd
    systemctl stop httpd
    systemctl status httpd
    systemctl enabel httpd (enabling in boot)
