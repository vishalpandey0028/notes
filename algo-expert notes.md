
# Into To Data Structures

Definition-->???

- Complexity Analysis
    1. Time Conlpexity -- how fast an algorithms is
    2. Space Complexity -- how much space an algorithm takes

- Memory
  - endianness--??(most significant byte is to the left)
    memory slots are of 8 bits
    32 bit integers would take 4 contigeous slots
    memory slot is represented by an memory address
    character encoding --- ???
    memory slot access is direct(in expensive operation/ basic in terms of algorithm)
    memory slots are limited which leads to space efficient algorithm
    pointer -- store address of a memory slot in a slot(in direct access to data)

- Big O Notation
    Measuring change in speed of algorithm with respect to size of input
    Asysmtotic analysis 
    Ex:- a=[....] -- N is lenght of the array
  - F1(a) => 1+a[0] => O(1)
  - F2(a) => sum(a) => O(N)
  - F3(a) => pair(a) => O(N^2)

- Logarithm
    log b(N)=y ==> b^y = N (in computer algorithm b=2)
    log N = y ==> 2^y = N
    if cutting input size in half then we are dealing log complexity algorithm

- Arrays
    Static and Dynamic Arrays
    operations
      - read - O(1) both time and space
      - update - O(1) "
      - create/initialize - O(N) both time and space
      - traverse/ interate - O(N)T, O(1)S
      - Copy - O(N) T and S
      - inserting/ adding a new element
          - O(N)T, O(1)S (Static Array)
          - o(N)T(worst) O(1)T best, (Dynamic Array) (Amortized Analysis)
      - pop
