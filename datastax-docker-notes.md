# Intro to Docker

[Datastax Docker intro week 1](https://github.com/datastaxdevs/docker-learning-path/blob/master/week-1/docker-slides-week-1.pdf)  
Docker - Build once and run everywhere

Its a set of tool to build, ship  and run any soft. with all dependencies at ease

Containers vs virtual machines
![comparison docker con vs vm](/images/vm-vs-containers.PNG)

Docker is orginally based on LXC(LinuX containers)

Isolation is less in Docker when compared to Virtual machines

- Image
  Its as immutable readonly template Containers( Like Class for object in OOPs)
  following can be performed with an image:
  - Build image
    Dockerfile is used to build an image(contains set of instruction for Docker to build an image )
    Commonly Instructions used in DockerFile:
    - FROM (to specify base image)
    - COPY
    - RUN (system commands to run while building the image)(used to setup image)
    - CMD (commands to run while starting an comtainer)

    Example dockerfile

    Example 1
    ```dockerfile
    FROM debian
    RUN apt-get upadate
    CMD echo Hello World
    ```

    Example 2
    ```dockerfile
    FROM ubuntu
    RUN apt-get update && apt-get install -y nginx
    RUN ln -sf /dev/stdout /var/log/nginx/access.log
    RUN ln -sf /dev/stderr /var/log/nginx/error.log
    # copying index.html from pwd to built image
    COPY index.html /var/www/html/index.nginx-debian.html 
    # deamon off asking docker to run comtainer in foreground
    CMD ["nginx", "-g", "daemon off;"] 
    ```

    Example 3
    ```dockerfile
    FROM python:3-alpine
    # set current working directory inside container
    WORKDIR /usr/src/app
    COPY . .
    RUN pip install --no-cache-dir -r requirements.txt
    # set environment varaiables of container
    ENV FLASK_APP=app.py FLASK_ENV=development
    CMD [ "flask", "run", "--host=0.0.0.0", "--port=80" ]
    ```

    Build image using `docker build .` (asking docker engine to search dockerfile in current directory)
    docker engine works as Server client. Docker command we use build/run/etc containers/image is docker client which sends docker daemon

    docker command to list out all image `docker images`

  - Launch Container with image
    - Run image `docker run <image ID>`
    - get all running container instances `docker ps`
    - `docker run --publish <host port>:<container port> <image>`(binding container port to host)
    - `docker run --publish 80:80 --detach <imageid>` (asking docker to detach from cmd while running container)
    - `docker run -p 80:80 -e FLASK_ENV=productio <imageid of example3>` set env values of container
    - `docker logs <container id>` get logs of a container
    - `docker logs --follow <container id>` to get realtime logging using --follow or -f
    - `docker stop/kill <containerid>`

  - Delete an Image

- Container
  Instantiated image ,a process in memory. Running entity
  - Start it
  - Stop it
  - Run Commands in it
  - Monitor Logs

- Shipping Image
  Docker Registry is used to store image for future and share it with others  
  hub.docker.com commonly used docker registry
  - `docker pull <image name>` to get images from registry
  - `docker build --tag\-t <repositrypath from registry>:tagname/version .`
  - `docker push imagenmae/ <repositrypath from registry>:tagname/version`
